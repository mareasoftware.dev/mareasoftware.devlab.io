# mareasoftware.dev

![CI/CD Master](https://gitlab.com/mareasoftware.dev/mareasoftware.dev.gitlab.io/badges/master/pipeline.svg)

Coming soon! :rocket:

See you at [gitlab.mareasoftware.dev](https://gitlab.mareasoftware.dev)

```shell
# Fork
# git clone ...
git config core.hooksPath ./script/githooks
```

Use your static-serve tool of choice to develop, i.e. `budo`

```shell
npm install -g budo
budo --dir static/ --live
```
